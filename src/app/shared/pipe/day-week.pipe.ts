import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dayWeek'
})
export class DayWeekPipe implements PipeTransform {
  private authorities: any = {
    Mon: {name: 'Пн'},
    Tue: {name: 'Вт'},
    Wed: {name: 'Ср'},
    Thu: {name: 'Чт'},
    Fri: {name: 'Пт'},
    Sat: {name: 'Сб'},
    Sun: {name: 'Вс'}
  };

  transform(value: string): string {
    if (value !== null && value !== undefined) {
      return this.authorities[value].name;
    } else {
      return '';
    }
  }

}
