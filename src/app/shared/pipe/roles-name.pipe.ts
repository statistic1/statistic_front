import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'rolesName'
})
export class RolesNamePipe implements PipeTransform {
  private rolesName: any = {
    ADMIN: {name: 'Администратор'},
    USER: {name: 'Пользователь'},
    ROS_TOURISM: {name: 'Ростуризм'},
    RBN_SKILL: {name: 'РБН_СКИЛЛ'}
  };

  transform(value: string): string {
    return this.rolesName[value].name;
  }
}
