import {StatImpl} from './stat-impl.model';
import {Moment} from 'moment';

export class StatRnb implements StatImpl {
    constructor(
        public rowDate?: Moment,
        public dayOfWeek?: string,
        public countAll?: number,
        public cans?: number,
        public cabn?: number,
        public sumTalkTime?: number,
        public avgTalTime?: number,
        public callToBank?: number,
        public sl?: number,
        public percABN?: number) {
        this.dayOfWeek = this.dayOfWeek ? this.dayOfWeek : '';
        this.countAll = this.countAll ? this.countAll : 0;
        this.cans = this.cans ? this.cans : 0;
        this.cabn = this.cabn ? this.cabn : 0;
        this.sumTalkTime = this.sumTalkTime ? this.sumTalkTime : 0;
        this.avgTalTime = this.avgTalTime ? this.avgTalTime : 0;
        this.callToBank = this.callToBank ? this.callToBank : 0;
        this.sl = this.sl ? this.sl : 0;
        this.percABN = this.percABN ? this.percABN : 0;
    }
}
