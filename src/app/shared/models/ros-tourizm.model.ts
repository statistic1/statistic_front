import {Moment} from 'moment';
import {StatImpl} from './stat-impl.model';

export class RosTourism implements StatImpl {
  constructor(
    public rowDate?: Moment,
    public dayOfWeek?: string,
    public makeRt8?: number,
    public rtToOther?: number,
    public returnRt11?: number,
    public returnRt05?: number,
    public makeOther?: number,
    public makeTula?: number) {
    this.dayOfWeek = this.dayOfWeek ? this.dayOfWeek : '';
    this.makeRt8 = this.makeRt8 ? this.makeRt8 : 0;
    this.rtToOther = this.rtToOther ? this.rtToOther : 0;
    this.returnRt11 = this.returnRt11 ? this.returnRt11 : 0;
    this.returnRt05 = this.returnRt05 ? this.returnRt05 : 0;
    this.makeOther = this.makeOther ? this.makeOther : 0;
    this.makeTula = this.makeTula ? this.makeTula : 0;
  }
}

