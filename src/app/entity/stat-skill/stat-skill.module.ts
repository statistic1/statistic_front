import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatSkillRoutingModule } from './stat-skill-routing.module';
import {StatisticModule} from '../statistic/statistic.module';
import { ViewTableComponent } from './view-table/view-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';


@NgModule({
  declarations: [ViewTableComponent],
  exports: [
    ViewTableComponent
  ],
  imports: [
    CommonModule,
    StatSkillRoutingModule,
    StatisticModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ]
})
export class StatSkillModule { }
