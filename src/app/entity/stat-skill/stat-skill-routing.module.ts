import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from '../../core/guard/auth-guard.service';
import {StatSkillComponent} from './stat-skill.component';

const routes: Routes = [
  {
    path: 'statSkill',
    data: {
      authorities: ['USER', 'ADMIN', 'RBN_SKILL'],
    },
    children: [
      {path: '', component: StatSkillComponent}],
    canActivate: [AuthGuardService],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatSkillRoutingModule {
}
