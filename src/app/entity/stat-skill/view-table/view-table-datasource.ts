import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {map} from 'rxjs/operators';
import {Observable, of as observableOf, merge} from 'rxjs';
import {StatRnb} from '../../../shared/models/stat-rnb.model';

/**
 * Data source for the ViewTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ViewTableDataSource extends DataSource<StatRnb> {
  data: StatRnb[];
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<StatRnb[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect(): void {
  }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: StatRnb[]): any {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: StatRnb[]): StatRnb[] {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'rowDate' :
          return compare(+a.rowDate, +b.rowDate, isAsc);
        case 'dayOfWeek' :
          return compare(+a.dayOfWeek, +b.dayOfWeek, isAsc);
        case 'countAll' :
          return compare(a.countAll, b.countAll, isAsc);
        case 'cans' :
          return compare(a.cans, b.cans, isAsc);
        case 'cabn' :
          return compare(a.cabn, b.cabn, isAsc);
        case 'sumTalkTime' :
          return compare(a.sumTalkTime, b.sumTalkTime, isAsc);
        case 'avgTalTime' :
          return compare(a.avgTalTime, b.avgTalTime, isAsc);
        case 'callToBank' :
          return compare(a.callToBank, b.callToBank, isAsc);
        case 'sl' :
          return compare(a.sl, b.sl, isAsc);
        case 'percABN' :
          return compare(a.percABN, b.percABN, isAsc);
        default:
          return 0;
      }
    });
  }

  setData(statList: StatRnb[]): void {
    this.data = statList;
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean): any {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
