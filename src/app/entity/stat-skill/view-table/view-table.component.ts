import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable} from '@angular/material/table';
import {ViewTableDataSource} from './view-table-datasource';
import {StatRnb} from '../../../shared/models/stat-rnb.model';

@Component({
  selector: 'app-view-table',
  templateUrl: './view-table.component.html',
  styleUrls: ['./view-table.component.scss']
})
export class ViewTableComponent implements AfterViewInit, OnInit {
  @Input()
  dataSource: ViewTableDataSource;
  @Input()
  total: StatRnb = new StatRnb();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<StatRnb>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'rowDate',
    'dayOfWeek',
    'countAll',
    'cans',
    'cabn',
    'sumTalkTime',
    'avgTalTime',
    'callToBank',
    'sl',
    'percABN'
  ];

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
