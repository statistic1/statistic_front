import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatSkillComponent } from './stat-skill.component';

describe('StatSkillComponent', () => {
  let component: StatSkillComponent;
  let fixture: ComponentFixture<StatSkillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatSkillComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
