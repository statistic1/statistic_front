import {Component, OnInit} from '@angular/core';
import {StatRnb} from '../../shared/models/stat-rnb.model';
import {FormBuilder, Validators} from '@angular/forms';
import {HttpParams} from '@angular/common/http';
import * as moment from 'moment';
import {StatisticService} from '../statistic/statistic.service';
import {ViewTableDataSource} from './view-table/view-table-datasource';

@Component({
  selector: 'app-stat-skill',
  templateUrl: './stat-skill.component.html',
  styleUrls: ['./stat-skill.component.scss']
})
export class StatSkillComponent implements OnInit {
  statList: StatRnb[] = [];
  statRnb: StatRnb = new StatRnb();
  isVisibleView = false;
  statisticService: StatisticService;
  reportNames: any = [{value: 'rbnSkill40', name: 'РНБ Скилл 40'}, {value: 'rbnSkill41', name: 'РНБ Скилл 41'}];
  filterForm = this.fb.group({
    filterName: [null, [Validators.required]],
    dateStart: [null],
    dateEnd: [null]
  });
  dataSource: ViewTableDataSource;

  constructor(private fb: FormBuilder, statisticService: StatisticService) {
    this.statisticService = statisticService;
  }

  ngOnInit(): void {
  }

  submitFilter(): void {
    this.findStat();
  }

  clearFilter(): void {
    this.isVisibleView = false;
    this.filterForm.patchValue({
      filterName: null,
      dateStart: null,
      dateEnd: null
    });
  }

  findStat(): void {
    this.dataSource = new ViewTableDataSource();
    this.isVisibleView = false;
    this.statList = [];
    this.statRnb = new StatRnb();
    let options: HttpParams = new HttpParams();

    if (this.filterForm.get(['filterName']).value) {
      options = options.set('filterName', this.filterForm.get(['filterName']).value);
    }
    const dateStart = this.filterForm.get(['dateStart']).value;
    if (dateStart && moment(dateStart).isValid()) {
      options = options.set('dateStart', dateStart);
    }
    const dateEnd = this.filterForm.get(['dateEnd']).value;

    if (dateEnd && moment(dateEnd).isValid()) {
      options = options.set('dateEnd', dateEnd);
    }
    this.statisticService.findStatisticRosTourism(options)
      .subscribe(res => {
        console.log('res.body = ');
        console.log(res.body);
        console.log(res.body.length);
        this.summCount(res.body);
        console.warn('summa');
        console.warn(this.statRnb);
        this.statList = res.body;
        this.dataSource.setData(this.statList);
        this.isVisibleView = true;

      }, error => {
        console.warn('у нас проблемы лю-лю');
      });
  }

  summCount(list: StatRnb[]): void {
    if (list !== undefined && list != null) {
      list.forEach(e => {
        this.statRnb.countAll = this.statRnb.countAll + e.countAll;
        this.statRnb.cans = this.statRnb.cans + e.cans;
        this.statRnb.cabn = this.statRnb.cabn + e.cabn;
        this.statRnb.sumTalkTime = this.statRnb.sumTalkTime + e.sumTalkTime;
        this.statRnb.avgTalTime = this.statRnb.avgTalTime + e.avgTalTime;
        this.statRnb.callToBank = this.statRnb.callToBank + e.callToBank;
        this.statRnb.sl = this.statRnb.sl + e.sl;
        this.statRnb.percABN = this.statRnb.percABN + e.percABN;
      });
      this.statRnb.percABN = (this.statRnb.cabn * 100) / this.statRnb.countAll;
    }
  }
}
