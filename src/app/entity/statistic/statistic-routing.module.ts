import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StatisticComponent} from './statistic.component';
import {AuthGuardService} from '../../core/guard/auth-guard.service';

const routes: Routes = [
  { path: 'statRos',
    data: {
      authorities: ['USER', 'ADMIN', 'ROS_TOURISM'],
    },
    children: [
      {path: '', component: StatisticComponent}],
    canActivate: [AuthGuardService],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatisticRoutingModule { }
