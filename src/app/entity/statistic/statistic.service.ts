import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {RosTourism} from '../../shared/models/ros-tourizm.model';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {Observable} from 'rxjs';
import {AuthService} from '../../core/auth/auth.service';
import {StatImpl} from '../../shared/models/stat-impl.model';

@Injectable({
  providedIn: 'root'
})
export class StatisticService {
  private rootUrl: string = GLOBAL_URL + '/api';
  private url: string = GLOBAL_URL + '/api/findStatistic';
  constructor(private http: HttpClient, protected authService: AuthService) {
  }

  public findStatisticRosTourism(options: HttpParams): Observable<HttpResponse<StatImpl[]>> {
    return this.http.get<RosTourism[]>(this.url,  {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }

}
