import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatFooterRow, MatTable} from '@angular/material/table';
import {DataTableDataSource} from './data-table-datasource';
import {RosTourism} from '../../../shared/models/ros-tourizm.model';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements AfterViewInit, OnInit {
  @Input()
  dataSource: DataTableDataSource;
  @Input()
  total: RosTourism;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator) footer: MatFooterRow;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<RosTourism>;

  constructor() {
  }

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'rowDate',
    'dayOfWeek',
    'makeRt8',
    'rtToOther',
    'returnRt11',
    'returnRt05',
    'makeOther',
    'makeTula'
  ];


  ngOnInit(): void {
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    // this.table.dataSource = this.dataSource;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    // this.dataSource.matFooterRowDef
    this.table.dataSource = this.dataSource;
  }
}
