import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatisticRoutingModule } from './statistic-routing.module';
import { StatisticComponent } from './statistic.component';
import {DayWeekPipe} from '../../shared/pipe/day-week.pipe';
import {ReactiveFormsModule} from '@angular/forms';
import { DataTableComponent } from './data-table/data-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';


@NgModule({
  declarations: [StatisticComponent, DayWeekPipe, DataTableComponent],
    imports: [
        CommonModule,
        StatisticRoutingModule,
        ReactiveFormsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
    ],
    exports: [DayWeekPipe]
})
export class StatisticModule { }
