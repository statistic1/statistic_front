import {Component, OnInit} from '@angular/core';
import {StatisticService} from './statistic.service';
import {RosTourism} from '../../shared/models/ros-tourizm.model';
import {HttpParams} from '@angular/common/http';
import {FormBuilder} from '@angular/forms';
import * as moment from 'moment';
import {DataTableDataSource} from './data-table/data-table-datasource';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {
  rosTourismList: RosTourism[] = [];
  statisticService: StatisticService;
  rosTourismRes: RosTourism = new RosTourism();
  isVisibleView = false;
  datasource: DataTableDataSource;

  constructor(private fb: FormBuilder, statisticService: StatisticService) {
    this.statisticService = statisticService;
  }

  filterForm = this.fb.group({
    dateStart: [null],
    dateEnd: [null]
  });

  ngOnInit(): void {
    this.findStat();
  }

  findStat(): void {
    this.isVisibleView = false;
    this.rosTourismList = [];
    this.rosTourismRes = new RosTourism();
    this.datasource =  new DataTableDataSource();
    let options: HttpParams = new HttpParams();
    options = options.set('filterName', 'rosTourism');
    const dateStart = this.filterForm.get(['dateStart']).value;
    if (dateStart && moment(dateStart).isValid()) {
      options = options.set('dateStart', dateStart);
    }
    const dateEnd = this.filterForm.get(['dateEnd']).value;

    if (dateEnd && moment(dateEnd).isValid()) {
      options = options.set('dateEnd', dateEnd);
    }
    this.statisticService.findStatisticRosTourism(options)
      .subscribe(res => {
        console.log('res.body = ');
        console.log(res.body);
        console.log(res.body.length);
        this.summCount(res.body);
        console.warn('summa');
        console.warn(this.rosTourismRes);
        this.rosTourismList = res.body;
        this.datasource.setData(this.rosTourismList);
        this.isVisibleView = true;
      }, error => {
        console.warn('у нас проблемы лю-лю');
      });
  }

  submitFilter(): void {
    this.findStat();
  }

  clearFilter(): void {
    this.isVisibleView = false;
    this.filterForm.patchValue({
      dateStart: null,
      dateEnd: null
    });
    this.findStat();
  }

  summCount(list: RosTourism[]): void {
    if (list !== undefined) {
      list.forEach(e => {
        this.rosTourismRes.makeTula = this.rosTourismRes.makeTula + e.makeTula;
        this.rosTourismRes.makeOther = this.rosTourismRes.makeOther + e.makeOther;
        this.rosTourismRes.returnRt05 = this.rosTourismRes.returnRt05 + e.returnRt05;
        this.rosTourismRes.returnRt11 = this.rosTourismRes.returnRt11 + e.returnRt11;
        this.rosTourismRes.rtToOther = this.rosTourismRes.rtToOther + e.rtToOther;
        this.rosTourismRes.makeRt8 = this.rosTourismRes.makeRt8 + e.makeRt8;
      });
    }
  }

}
