import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {StatisticModule} from './entity/statistic/statistic.module';
import {HttpClientModule} from '@angular/common/http';
import {AdminRoutingModule} from './admin/admin-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {UserModule} from './entity/user/user.module';
import { StatSkillComponent } from './entity/stat-skill/stat-skill.component';
import {StatSkillModule} from './entity/stat-skill/stat-skill.module';
import {ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    StatSkillComponent,
  ],
  imports: [
    UserModule,
    NgbModule,
    AdminRoutingModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StatisticModule,
    StatSkillModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  // exports: [DayWeekPipe],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
