import { Component, OnInit } from '@angular/core';
import {LoginComponent} from '../admin/login/login.component';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../core/auth/auth.service';
import {RegistryComponent} from '../admin/registry/registry.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  route: Router;
  constructor(public modalService: NgbModal, public authService: AuthService, route: Router
  ) {
    this.route = route;
  }

  ngOnInit(): void {
  }

  login(): void {
    const modelRef = this.modalService.open(LoginComponent, {size: 'md', backdrop: 'static'});
  }

  registry(): void {
    const modalRef = this.modalService.open(RegistryComponent, {size: 'md', backdrop: 'static'});
  }

  logout(): void {
    this.authService.clearJWTToken();
    this.route.navigate(['']);
  }
}
